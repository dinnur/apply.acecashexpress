## Proof of Concept by sending requests to [apply.acecashexpress.com](https://apply.acecashexpress.com/lending/payday-loans/ForgotLoginInformation) and passing server validation

---
> There are two forms on the page, both of which contain an invisible and already filled field "__RequestVerificationToken", which stores csrf_token confirming to the server that the request is sent only on behalf of the given form in which such a token is located. Additionally, along with this, another csrf_token token is sent to the request cookies confirming the page and the session token
>> All this data is available when sending a "GET" request to the page [ForgotInformation](https://apply.acecashexpress.com/lending/payday-loans/ForgotLoginInformation)
---
> In addition, it is required to send one more session token, which I have not yet had time to get acquainted with more closely. But I managed to find a way to get it on this endpoint of server [session_url](https://app.trustev.com/api/v2.0/session)

---
> In the technical part, I created a class when initializing the object of which I automatically send a "GET" request to the form page, where after I pull out the data I need and assign the object's attributes to this data as links to them. 
>> The used endpoints are available in the py/urls.py file
>> The class are available in the file py/site_urls.py

[Description of class attributes and main methods](https://drive.google.com/file/d/1LYe4mlP_xoLKyi4bxDmleFwjrzq73mGU/view?usp=sharing)

---

### Sending request to forgot_password takes next row data:
```text
# payload
__RequestVerificationToken=${csrf_input}
EmailAddress=${email}
pgRenderTime=${render_time}
pgSubmitTime=
OriginalEmailAddress=
TrustevSessionId=${session}

# cookie
__RequestVerificationToken_L2xlbmRpbmc1=${csrf_token_cookie}
ASP.NET_SessionId=${asp_net_id}
```
