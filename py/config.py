from py.utils import filter_headers


PROXIES = {'http': 'http://5.189.151.227:24143'}

DOMAIN = 'apply.acecashexpress.com'

API_KEY = 'EBY6H-JQUQV-HBGHH-V82S8-BHRLC'
X_PUBLICKEY = 'eb6707ba6b7d45a1aae4a91fbd73ccf1'
WEBSOCKET_KEY = 'a70vx+ufQd62FSSJuIl6Nw=='
APP_ID = 'd65921b2-8e68-4ce4-bca8-e9340c0ca8cc'

TUID = '39c3a930-36a0-4815-b02a-a0bfa3242d0e'

AKAMAI_AI = '439048'

HEADERS_DEFAULT = {
    'authority': 'apply.acecashexpress.com',
    'accept': 'application/json, text/javascript, */*; q=0.01',
    'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) '
                  'AppleWebKit/537.36 (KHTML, like Gecko) '
                  'Chrome/97.0.4692.71 Safari/537.36',
    'origin': 'https://apply.acecashexpress.com',
    'referer': 'https://apply.acecashexpress.com/',
    'Accept-Language': 'en-US,en;q=0.9,ru;q=0.8'
}

PAYLOAD_FORM = "__RequestVerificationToken={csrf_token}" \
               "&EmailAddress={email}&" \
               "pgRenderTime={render_time}" \
               "&pgSubmitTime=&" \
               "OriginalEmailAddress=" \
               "&TrustevSessionId={session_id}"


headers = filter_headers(HEADERS_DEFAULT, {
    'Connection': 'keep-alive',
    'X-PublicKey': 'eb6707ba6b7d45a1aae4a91fbd73ccf1',
    'Content-Type': 'application/json',
    'X-TU-DV-JS-Version': 'v3.9.27645',
})

