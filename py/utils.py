def filter_headers(default_headers: dict, headers_: dict):
    new_headers = default_headers | headers_
    default_headers_keys = default_headers.keys()
    for key in headers_.keys():
        if key not in default_headers_keys and key.lower() in default_headers_keys:
            new_headers.pop(key.lower())
    return new_headers
