import functools
from typing import Callable

import httpx


def return_json(func: Callable):
    @functools.wraps(func)
    def return_if_200(self, *args, **kwargs):
        response = func(self, *args, **kwargs)
        if response is None:
            return
        if response and response.status_code == httpx.codes.OK:
            return response.json()
        print(response.text)
        print(response.status_code)
    return return_if_200

