import json
from datetime import datetime
from time import sleep

import httpx
from lxml import html

from decorators import return_json
from config import HEADERS_DEFAULT, headers, PROXIES, PAYLOAD_FORM, DOMAIN
from urls import SESSION_URL, FORGOT_EMAIL_URL, SEARCH_BY_EMAIL, FORGOT_PAGE_URL


class ForgotPassword:
    xpath = {
        'csrf_input': '//div[@id="ForgotPassword"]//input[@name="__RequestVerificationToken"]/@value',
    }

    def __init__(self):
        self.client = httpx.Client(headers=HEADERS_DEFAULT, timeout=None, proxies=PROXIES['http'])
        self.session: str = None
        self._save_csrf_token()
        self._save_session()

    def _save_session(self):
        resp_data = self._get_session_data
        if resp_data and resp_data.get('SessionId'):
            self.session = resp_data['SessionId']

    def _save_csrf_token(self):
        resp = self.client.get(FORGOT_PAGE_URL)
        self.csrf_token_cookie = resp.cookies.get('__RequestVerificationToken_L2xlbmRpbmc1')
        self.asp_net_id = resp.cookies.get('ASP.NET_SessionId')
        if self.asp_net_id and self.csrf_token_cookie:
            self.client.cookies.set('__RequestVerificationToken_L2xlbmRpbmc1', self.csrf_token_cookie, domain=DOMAIN)
            self.client.cookies.set('ASP.NET_SessionId', self.asp_net_id, domain=DOMAIN)
            self.tree = html.fromstring(resp.text)
            for key, xpath in self.xpath.items():
                # [0] because getting the first form of two forms
                setattr(self, key, self.tree.xpath(xpath)[0])
            self.render_time = datetime.now().strftime("%Y/%m/%d %H:%M:%S")

    @return_json
    def search_by_email(self, email: str):
        if self.csrf_token_cookie:
            payload = {'__RequestVerificationToken': self.csrf_input,
                       'emailAddress': email}
            return self.client.post(SEARCH_BY_EMAIL, data=payload)

    @return_json
    def requests_to_forgot_password(self, email: str):
        if self.csrf_input and self.session and self.asp_net_id and self.csrf_token_cookie:
            payload = PAYLOAD_FORM.format(
                email=email,
                session_id=self.session,
                csrf_token=self.csrf_input,
                render_time=self.render_time
            )
            return self.client.post(FORGOT_EMAIL_URL, content=payload)

    @property
    @return_json
    def _get_session_data(self):
        return self.client.post(SESSION_URL, headers=headers, content=json.dumps({}))


site = ForgotPassword()

print(site.csrf_input)
print(f'SESSION_ID: {site.session}',
      f'\nRENDER_TIME: {site.render_time}',
      f'\nCSRF_FORM_TOKEN: {site.csrf_input}',
      f'\nCSRF_COOKIE: {site.csrf_token_cookie}',
      f'\nASP.NET ID: {site.asp_net_id}')
sleep(2)  # in order not to create a load on the server
print(site.search_by_email('add213@gmail.com'))
sleep(2)
print(site.requests_to_forgot_password(email='add213@gmail.com'))
site.client.close()

# TODO: Check with registered email
