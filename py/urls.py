from py.config import API_KEY, DOMAIN, AKAMAI_AI


URL_OF_CONF_FORM = "https://c.go-mpulse.net/api/config.json" \
                   "?key={api_key}&d={domain}&ak.ai={akamai_ai}"

DETAIL_URL = 'https://app.trustev.com/api/v2.0/Session/{session_id}/detail'

DEVICE_URL = 'https://app.trustev.com/api/v2.0/device'

CONFIG_URL = URL_OF_CONF_FORM.format(api_key=API_KEY, domain=DOMAIN, akamai_ai=AKAMAI_AI)

FORGOT_PAGE_URL = "https://apply.acecashexpress.com/lending/payday-loans/ForgotLoginInformation"
FORGOT_EMAIL_URL = "https://apply.acecashexpress.com/lending/payday-loans/ForgotPassword/"

SESSION_URL = "https://app.trustev.com/api/v2.0/session"

SEARCH_BY_EMAIL = "https://apply.acecashexpress.com/lending/payday-loans/AccountLookupByEmail"
